<?php

header('Content-Type: application/json');

$url = 'http://localhost/';
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_TIMEOUT,10);
$output = curl_exec($ch);
$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);
  
$arg = '{"status":"ok"}';
$arg_pretty = json_encode(json_decode($arg), JSON_PRETTY_PRINT);

if ($httpcode == 200) {
	echo $arg_pretty;
  }

else {
  echo "ERROR!";
  }

?>