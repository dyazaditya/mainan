#TO RUN COMMAND USE THIS
#sudo sh docker-script.sh 0.0.1

#TO CLEANUP RUNNING CONTAINER
#sudo docker rm --force $(sudo docker ps -a -q)

# Variable Global
TESTING_BUILD_VERSION=$1
# Variable for testing
TESTING_PUBLISH_PORT=80

IMAGE_NAME=althafaraz/testing
CONTAINER_NAME=testing

#Pull Script
#testing image pull
docker pull ${IMAGE_NAME}:${TESTING_BUILD_VERSION}

#Deployment Script
#Deploy testing
set -x
docker stop ${CONTAINER_NAME}
docker rm ${CONTAINER_NAME}

set -e
docker run -d --name ${CONTAINER_NAME} \
    -p ${TESTING_PUBLISH_PORT}:80 \
    -h ${CONTAINER_NAME} \
    --restart unless-stopped \
    ${IMAGE_NAME}:${TESTING_BUILD_VERSION}
sleep 10s
docker logs ${CONTAINER_NAME}